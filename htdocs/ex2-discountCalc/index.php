<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <title>Discount Calculator</title>
  </head>
  <body>
		
	<div class="container">
		<form action="index.php" method="POST">
			<div class="form-group">
				<label >Price</label>
				<input  name="price" type="number" class="form-control" >
			</div>
			
			<div class="form-group">
				<label >Discount Rate</label>
				<input name="rate"  type="number" class="form-control" >
			</div>
			
			<button name="submit" type="submit" class="btn btn-primary">Submit</button>
		</form>
	</div>
	
	
	<?php
		if(isset($_POST['submit'] )){
			echo "Form submitted<br/>";
			
			var_dump($_POST);
			
			$price = $_POST["price"];
			$rate = $_POST["rate"];
			
			$discount = ($rate / 100) * $price;
			$finalPrice = $price - $discount;
			
			echo "<br/>Discout value: $finalPrice";
			
		}else {
			echo "Please enter both inputs";
		}
	?>
	
	
	
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>