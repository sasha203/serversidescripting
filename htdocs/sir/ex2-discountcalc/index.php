<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <title>Discount Calculator</title>
  </head>
  <body>
  
    <div class="container">
		<h1 class="jumbotron">Discount Calculator</h1>
		<hr/>
		<div class="row">
			<div class="col">
				<form action="index.php" method="POST">
				  <div class="form-group">
					<label for="price">Price (&euro;)</label>
					<input type="number" step="0.01" class="form-control" name="price" id="price" value="" placeholder="Enter price (e.g. 200.30)">
				  </div>
				  <div class="form-group">
					<label for="rate">Discount rate (%)</label>
					<input type="number" class="form-control" id="rate" name="rate" value="" placeholder="Discount rate (e.g. 10)">
				  </div>
				  <button type="submit" name="submit_btn" class="btn btn-primary">Calculate Discounted Price!</button>
				</form>
			</div>
			<div class="col">
				<div class="card">
				  <div class="card-header">
					<h2 class="card-title">Discounted price</h2>
				  </div>
				  <div class="card-body">
					
					<p class="card-text">
						<?php
							if (isset($_POST['submit_btn'])) {
								echo "Form submitted!<hr>";
								
								//check what's inside $_POST (array with values from form)
								//var_dump($_POST);
								
								//get price and rate values from form
								$price = intval($_POST['price']);
								$rate = intval($_POST['rate']);
								
								if (!empty($price) && !empty($rate)) {
									//calculation
									$discount = ($rate/100)*$price;
									$final = $price - $discount;
									
									echo "<hr/>";
									echo "Price: &euro;$price<br/>";
									echo "Discount rate: $rate%<br/>";
									echo "Discount: &euro;$discount<br/>";
									echo "<hr/>";
									echo "Final discounted price: &euro;".$final."<br/>";
									
								}
								else {
									echo "Please enter the price and the discount rate!";
								}								
								
							}
							else {
								echo "Please submit the form.";
							}
						?>
					</p>
				  </div>
				</div>
			</div>
		</div>
	</div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>