<!doctype html>
<html>
	<head>
		<title>PHP: Hello</title>
	</head>
	
	<body>
		<?php
			//this is a comment
			//localhost:8084/ex1/index.php  will show this page.
			//RULE: every statement should end with a ;
			echo "Hello world!<br/>";	
			print "test<br/>";
			
			echo "<br/>";
			/*
			Multi-line comment
			*/
			
			$name = "Joe";
			$age = 24;
			$developer = true;
			$weight = 74.2;
			
			//will display the data type, length and value of the variable inputted.
			var_dump($name);
			echo "<hr/>";
			var_dump($age);
			echo "<hr/>";
			var_dump($developer);
			echo "<hr/>";
			var_dump($weight);
			echo "<hr/>";
			
			//Constants
			define("VAT_RATE", 0.18);
			
			echo "The current VAT rate is " . VAT_RATE;   //concatination char is a . not a +
			
			//$sign is a variable
			//var cannot begin with a number.
			//PHP is case sensitive.
			
			$mobile_phone_wvat = 399.00;
			$mobile_phone_vat = $mobile_phone_wvat * VAT_RATE;
			echo "<br/>The VAT Amount on the mobile phone is $mobile_phone_vat";
			
			$mobile_phone_total = $mobile_phone_wvat + $mobile_phone_vat;
			echo "<br/>The total price is: $mobile_phone_total";
			echo "<hr/>";
			
			//flokk += tuza .=
			
			// == will only check for value
			// === will check both for data type and value 
			
			
		
			
			
		?>
	</body>
</html>