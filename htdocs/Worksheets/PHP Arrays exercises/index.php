<!DOCTYPE html>
<html>
	<head>
		<title>PHP Arrays</title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	</head>
	
	<body>
		<div class="jumbotron container-fluid">
			<?php
			//1
				$months = ["Jan", "Feb", "Mar", "Apr", "May", "June", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"];
				$output = "Months in English: ";
				$i = 0;
				
				foreach($months as $month){
					if( $i == count($months) -1){
						$output .= " $month.";
					} 
					else {
						$output .= " $month, ";
					}
					
					
					$i++;
				}
				
				echo "$output<br/><br/>";
				
				
			?>
		</div>
	
		<div class="jumbotron">
			<?php
			//2
				$colors = ["Black", "Yellow", "White"];
				$output_colors = "Colors: ";
				$counter = 0;
				
				
				foreach( $colors as $color) {
					if($counter == count($colors)-1)
						$output_colors .= "$color";
					else
						$output_colors .= "$color, ";
					
					
					$counter++;
				}
				echo "$output_colors<br/><br/>";
				
				sort($colors);

				foreach($colors as $color) {
					echo "	<ul class=\"list-group\">
								<li>$color</li>
							</ul>";
				}
				
				
				array_push($colors, "Maroon", "Red" , "Orange");
				//print_r($colors);
				
				sort($colors);  //Sort is case sensitvie (caps are sorted first)
				echo "<br/><br/>";
				
				
				foreach($colors as $color) {
					echo "	<ul class=\"list-group\">
								<li>$color</li>
							</ul>";
				}
			?>
		</div>
		
		<div class="jumbotron">
			<?php
			//3  DI MHUX QEDA TAJBA NICEKJA MAL SS. (GHAX QEDA FUQ HOBBY 1)
				$hobbies = ["Football", "Gaming", "golf", "Tennis"];
				
				if(!isset($_POST['submit'])) {
					echo "
						<form method=\"post\" action=\"index.php\">
							<div class=\"form-group\">
								<label>Name: </label>
								<input type=\"text\" name=\"name\"/> <br/>
								
								<label>Surname: </label>
								<input type=\"text\" name=\"surname\"/> <br/>
								
								<label>DOB: </label>
								<input type=\"text\" name=\"dob\"/> <br/>
								
								<select name =\"hobby\"> 
						";
							
									foreach($hobbies as $hobby) {
										echo "<option value=\"$hobby\">" . $hobby . "</option>";
									}
									
					echo "		
								</select>
										
							</div>
									
							<input type= \"submit\" name=\"submit\" class=\"btn btn-primary\" value=\"Submit\" />
						</from>
						";
					
				}
				else { //Note if action was pointing to another page, instead of the else just write in the pointed php file.
					//Respone Section (Where action points)
					$name = $_POST['name'];
					$surname = $_POST['surname'];
					$dob = $_POST['dob'];
					$hobby = $_POST['hobby'];
					
					echo 
						"Welcome $name $surname, DOB: $dob, you have the following
						hobby : $hobby
					";
				}
					
					
				//var_dump($name);
			?>
		</div>
		
		<div class="jumbotron">
			<?php
			//4  MHUX TAHDEM SEW AQAS
			//var_dump($sports);
			
			$sports = ["Shaolin Kung fu", "Basketball", "Modern penthatlon"];
			echo 
				"There are various sports which one could practice, namely : ";
			
				foreach($sports as $sport) {
					echo "
						<ul>
							<li>$sport</li>
						</ul>
					";
				}
				
			
			
			if(!isset($_POST['go'])){
			echo
				"
				<form method=\"post\" action=\"index.php\">
					<input type=\"text\" name=\"addedItems\"/>
					<input type=\"submit\" name=\"go\" class=\"btn btn-primary\" value=\"Go\"/>
				</form>
				<br/><br/>
				<input type=\"text\" name=\"addedItems2\"/>
				<input type=\"submit\" class=\"btn btn-primary\" value=\"Add Items\"/>
			";	
			}
			else{
				$temp_inputs = [];
				var_dump($temp_inputs);
				
				/*
				foreach($temp_inputs  as $input) {
					//echo "$input";
				}*/
			}
			?>
		</div>	
		
		
		<div class="jumbotron">
			<?php
				//5
				$countries = ["Malta" => "Euro", "UK"=>"Sterling", "USA"=>"Dollars" ];
			
				
				if(!isset($_POST['sub5'])){
					echo "
						<form method=\"post\" action=\"index.php\">
							<label>Please choose a country:&nbsp;</label>
							<select name=\"country\">
							
					";
						foreach($countries as $country => $currency){
							echo "<option>$country </option>";
						}
					echo"
							</select>
							<br/>
							<input type=\"submit\" name=\"sub5\" class=\"btn btn-primary\" /> 
							
						</form>
					";	
				}
				else{
					$country = $_POST['country'];
					$currency = $countries[$country];
					
					echo "The currency in $country is $currency";
				}
			?>
		</div>
		
		<div class="jumbotron">
			<?php
			//6  MHUX LESTA
				$marks = [];
				
				function form_6(){
					echo "
							<form method=\"post\" action=\"index.php\">
								<label>Marks:&nbsp;</label>
								<input type=\"text\" name=\"mark\" />
								<input type=\"submit\" name=\"subMarks\" class=\"btn btn-primary\"/>
							</form>
						";	
				}
				
				
				if(!isset($_POST['subMarks'])){ 
					form_6();
				}
				else{
					form_6();
					array_push($marks, $_POST['mark']);
					var_dump($marks);
				}
					
					
								
				
			?>
		</div>
		
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	</body>
</html>