<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <title>More Basics</title>
  </head>
  <body>

	<div class="container">
		<h2>Arrays</h2>
		
		
		<?php
			//Associate array.
			$product = array ( "name" => "Oneplus 6T",
							   "price" => 600,
							   "images" => array(
									"oneplus_6t.jpg",
									"oneplus_6t_red.jpg"
							   ));
			foreach($product as $key => $x) { //key representing the key and x the value
				$dt = gettype($x);
				
				if($dt != "array")
					echo $key. ": " .$x . "<br/>";
				else{
					//foreach loop to go through internal array
					echo "images: ";
					foreach($x as $image_name) {
						echo $image_name . "<br/>";
					}
				}
			}
			echo "<hr/>";
			print_r($product);
			echo "<br/><br/>Total Products " . count($product); //NOTE WORKING
			echo "<hr/>";
			
			//indexed array
			//Both syntax work.
			$countries = ["Malta", "UK", "Italy", "France"];
			//$countries = array("Malta", "UK", "Italy", "France");
			
			print_r($countries);
			echo "<hr/>";
			

			//Creating a function
			function sum($array){
				$total = 0;
				foreach($array as $value){
					$total += $value;
				}
				
				return $total;
			}
			
			//Calling the function
			$ans = sum([2,1,3,4]);
			echo "The Answer is " . ($ans) . "<br/><br/>";
			
			
			
			$tableValue = 0;
			$t = 12; 
			for($i = 1; $i<13; $i++){
				$tableValue = $t * $i;
				echo $i . " X " .  $t . " = " . $tableValue . "<br/>";
			}
			
		?>
		
	</div>
	
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>