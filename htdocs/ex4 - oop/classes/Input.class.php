<?php
class Input extends Control {
	public $type;
	public $value;
	public $placeholder;
	
	public function __construct($_label, $_css_class, $_name, $_type = "text", 
	$_value = "", $_placeholder = "") {
		$this->label = $_label;
		$this->css_class = $_css_class;
		$this->name = $_name;
		$this->type = $_type;
		$this->value = $_value;
		$this->placeholder = $_placeholder;
	}
	
	public function draw() {
		$html = "<label for=\"{$this->name}\">{$this->label}</label>";
		
		//concatination
		$html .= "<input	type=\"{$this->type}\"   
							class=\"{$this->css_class}\"
							name=\"{$this->name}\"
							placeholder=\"{$this->placeholder}\"
							value=\"{$this->value}\"    />";
		return $html;
	}
}
?>