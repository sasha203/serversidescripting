<?php
	class Person {
		//attributes
		public $first_name;
		public $last_name;
		private $yob;
		private $date_employed;
		
		//this is the constructor (this is called when a new instance of a class is initilized)
		//method overloading doesn't exist instead if you want to make a parameter mandatory
		//the par should not be initialized or else make like follows $_first_name = ""
		
		//constructor (magic method)
		//two underscores !!
		public function __construct($_first_name = "", $_last_name = "", $_yob = ""){
			//echo "This is the constructor calling! ";
			$this->first_name = $_first_name;
			$this->last_name = $_last_name;
			$this->set_yob($_yob);
		} 
		
		//setter function for$yob (because it's private/)
		public function set_yob($_yob) {
			//set if parameter ($_yob) is greater than or equal to 1900 and less than or equal to current year
			if($_yob >= 1900 && $_yob <= date("Y")){
				//this.yob qed jipontja ghal al attribute ta person yob
				$this->yob = $_yob;
				
				//return true to show that it was successfully set
				return true;
			}
			//return false to show that it was not set.
			return false;
		}
		
		//getter
		public function get_yob(){
			return $this->yob;
		}
		
		public function set_date_employed($d, $m, $y) {
			//time, and date
			//returns the number of seconds from the given date.
			$this->date_employed = mktime(0,0,0,$m, $d, $y);
		}
		
		public function get_date_employed() {
			return $this->date_employed;
		}
		
		public function get_days_employed() {
			$today = time(); //Ghandek is secondi min 1 jan 1970 sa llum.
			$difference = $today - $this->date_employed;
			$diff_days = $difference/60/60/24;
			
			return (int)$diff_days;
		}
	}

?>