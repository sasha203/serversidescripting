<!doctype html>
<html>
	<head>
		<title>OOP</title>
	</head>
	
	<body>
		<h1>OOP</h1>
		
		<?php
			//include the class_alias
			require_once("classes/Person.class.php");
			
			
			$p1 = new Person;
			
			$p1->first_name = "Joe"; 
			$p1->last_name = "Borg";
			//we can no longer do this because it's private.
			//$p1->yob = -1;
			
			//we can however access the setter like so:
			$yob_set = $p1->set_yob(2000);
			
			if($yob_set)
				echo "Year of birth for p1 set.";
			else
				echo "Year of birth not set. Check value";
				
			
			echo "<br/>My first person is called {$p1->first_name} {$p1->last_name}";  
			
			
			echo "<br/> You were born on {$p1->get_yob()}";
			
			
			$p1->set_date_employed(1,10,2018);
			echo "<br/> Date employed: " .date('dS F Y', $p1->get_date_employed());
			
			
			echo "<br/> Date employed: " .$p1->get_days_employed();
			
			
			//$p2
			$p2 = new Person("cetta", "Borg", 1930);
		?>
		
	
			<h2>Form</h2>
		<?php
			require_once("classes/Control.class.php");
			require_once("classes/Input.class.php");
			
			$first_name = new Input ("First Name", "", "first_name", "","Please enter your first name");
			echo $first_name->draw(). "<br/>"; 
			
			$last_name = new input ("Last Name", "last_name" , "", "Please enter last name.");
			echo $last_name->draw();
			
			
			//Create a select instead of input. (do select class)
		?>
	</body>
</html>